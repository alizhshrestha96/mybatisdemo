package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.dao.UserDao;
import com.example.demo.domain.User;
import com.example.demo.mapper.UserMapper;

@SpringBootApplication
public class MybatisdemoApplication implements CommandLineRunner {

	private final UserDao userDao;

	private final UserMapper userMapper;

	public MybatisdemoApplication(UserDao userDao, UserMapper userMapper) {
//		super();
		this.userDao = userDao;
		this.userMapper = userMapper;
	}

	public static void main(String[] args) {
		SpringApplication.run(MybatisdemoApplication.class, args);
	}

	@Override
//	@SuppressWarnings("squid:S106")
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
//		System.out.println(this.userDao.selectUserById(2));
//		this.userDao.selectUsers().forEach(System.out::println);
		User user = new User("Unisha");
		userDao.insertUser(user);
	}

}
