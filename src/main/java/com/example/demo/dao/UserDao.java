package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import com.example.demo.domain.User;

@Component
public class UserDao {
	
	private final SqlSession sqlSession;

	public UserDao(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	public User selectUserById(int id) {
		return this.sqlSession.selectOne("selectUserById", id);
	}
	
	public List<User> selectUsers() {
		return this.sqlSession.selectList("findAllUsers");
	}
	
	public void insertUser(User user) {
		this.sqlSession.insert("insertUser", user);
	}
	
}
